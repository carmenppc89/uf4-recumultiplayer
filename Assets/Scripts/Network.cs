using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.EventSystems;

public class Network : NetworkBehaviour
{
    private NetworkAnimator networkAnimator;
    private Animator anim;
    public List<GameObject> targets = new List<GameObject>();
    public GameObject target;
    public bool killer = true;
    public NetworkVariable<bool> muerto = new NetworkVariable<bool>(false);
    private bool left = false;

    public List<RuntimeAnimatorController> controllers = new List<RuntimeAnimatorController>();
    [SerializeField] private List<Sprite> dedBodies = new List<Sprite>();
    public NetworkVariable<int> index = new NetworkVariable<int>();

    [SerializeField] private Transform dedPrefab;
    [SerializeField] private TextMeshProUGUI deadTxt;

    private void Awake()
    {
        this.anim = this.GetComponent<Animator>();
        this.networkAnimator = this.GetComponent<NetworkAnimator>();
    }



    private void Start()
    {
        this.Colores();
    }

    public void PlayAnimation(string triggerName)
    {
        anim.SetBool(triggerName, true);
    }

    void Update()
    {
        if (!IsOwner) return;

        if (!muerto.Value)
        {
            Vector3 moveDirection = Vector3.zero;

            if (Input.GetKey(KeyCode.W)) moveDirection.y += +1f;
            if (Input.GetKey(KeyCode.S)) moveDirection.y += -1f;
            if (Input.GetKey(KeyCode.A))
            {
                moveDirection.x += -1f;
                ChangeSideServerRpc(true);
            }
            if (Input.GetKey(KeyCode.D))
            {
                moveDirection.x += +1f;
                ChangeSideServerRpc(false);
            }
            float moveSpeed = 3f;

            Vector3 total = moveDirection * moveSpeed * Time.deltaTime;

            transform.position += total;

            if (killer)
            {
                if (Input.GetKeyUp(KeyCode.Mouse0) && this.target != null)
                {
                    Transform ded = Instantiate(dedPrefab);
                    ded.position = this.target.transform.position;
                    ded.GetComponent<SpriteRenderer>().sprite = dedBodies[this.target.GetComponent<Network>().index.Value];

                    ded.GetComponent<NetworkObject>().Spawn(true);

                    ClientDiesServerRpc(new ServerRpcParams());
                }
            }

            ChangeAnimServerRpc(total);

            TargetServerRpc();
        }
    }

    [ServerRpc]
    private void ClientDiesServerRpc(ServerRpcParams serverRpcParams)
    {
        target.GetComponent<Network>().muerto.Value = true;
    }

    public void SincronizarMatar(Network muerto)
    {
    }

    private void Colores()
    {
        foreach (Network player in FindObjectsOfType<Network>())
        {
            player.GetComponent<Animator>().runtimeAnimatorController = controllers[player.index.Value];
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            this.targets.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            this.targets.Remove(collision.gameObject);
        }
    }

    public void SetColor(int newColor)
    {
        index.Value = newColor;
    }

    private void OnColorChanged(int oldColor, int newColor)
    {
        GetComponent<Animator>().runtimeAnimatorController = controllers[index.Value];
    }

    private void OnMuerto(bool oldMuerto, bool newMuerto)
    {
        if (IsOwner)
        {
            deadTxt.gameObject.SetActive(true);
        }

        GetComponent<NetworkAnimator>().enabled = false;
        anim.enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        index.OnValueChanged += OnColorChanged;
        muerto.OnValueChanged += OnMuerto;
    }

    private void OnDestroy()
    {
        index.OnValueChanged -= OnColorChanged;
    }

    [ServerRpc]
    private void ChangeSideServerRpc(bool left)
    {
        this.left = left;
    }

    [ServerRpc]
    private void ChangeAnimServerRpc(Vector3 total)
    {
        if (total == Vector3.zero)
        {
            anim.SetBool("Walk", false);
        }
        else
        {
            anim.SetBool("Walk", true);
        }
        if (this.left)
        {
            anim.SetBool("L", true);
        }
        else
        {
            anim.SetBool("L", false);
        }
    }

    [ServerRpc]
    private void TargetServerRpc()
    {
        if (targets.Count != 0 && killer)
        {
            ClosestTargetServerRpc();
        }
        else
        {
            target = null;
        }
    }

    [ServerRpc]
    private void ClosestTargetServerRpc()
    {
        GameObject res = targets[0];

        float x = 0;
        float y = 0;
        float total = 0;

        if (Mathf.Abs(res.transform.position.x) > Mathf.Abs(this.transform.position.x))
        {
            x = Mathf.Abs(res.transform.position.x) - Mathf.Abs(this.transform.position.x);
        }
        else if (Mathf.Abs(res.transform.position.x) < Mathf.Abs(this.transform.position.x))
        {
            x = Mathf.Abs(this.transform.position.x) - Mathf.Abs(res.transform.position.x);
        }
        if (Mathf.Abs(res.transform.position.y) > Mathf.Abs(this.transform.position.y))
        {
            y = Mathf.Abs(res.transform.position.y) - Mathf.Abs(this.transform.position.y);
        }
        else if (Mathf.Abs(res.transform.position.y) < Mathf.Abs(this.transform.position.y))
        {
            y = Mathf.Abs(this.transform.position.y) - Mathf.Abs(res.transform.position.y);
        }
        total = Mathf.Abs(x) + Mathf.Abs(y);

        for (int i = 0; i < targets.Count; i++)
        {
            float x2 = 0;
            float y2 = 0;
            float total2 = 0;
            if (Mathf.Abs(targets[i].transform.position.x) > Mathf.Abs(this.transform.position.x))
            {
                x2 = Mathf.Abs(targets[i].transform.position.x) - Mathf.Abs(this.transform.position.x);
            }
            else if (Mathf.Abs(targets[i].transform.position.x) < Mathf.Abs(this.transform.position.x))
            {
                x2 = Mathf.Abs(this.transform.position.x) - Mathf.Abs(targets[i].transform.position.x);
            }
            if (Mathf.Abs(targets[i].transform.position.y) > Mathf.Abs(this.transform.position.y))
            {
                y2 = Mathf.Abs(targets[i].transform.position.y) - Mathf.Abs(this.transform.position.y);
            }
            else if (Mathf.Abs(targets[i].transform.position.y) < Mathf.Abs(this.transform.position.y))
            {
                y2 = Mathf.Abs(this.transform.position.y) - Mathf.Abs(targets[i].transform.position.y);
            }
            total2 = Mathf.Abs(x) + Mathf.Abs(y);
            if (total2 < total)
            {
                res = targets[i];
                x = x2;
                y = y2;
            }
        }
        this.target = res;
        print("Target mas cercano: " + this.target);
    }
}
