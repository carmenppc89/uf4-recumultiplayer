using System.Collections;
using System.Collections.Generic;
using System.Data;
using Unity.Netcode;
using Unity.Networking.Transport;
using Unity.VisualScripting;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    public List<int> availableColors;
    public List<int> assignedColors;
    public int players = 0;

    private void Start()
    {
        Unity.Netcode.NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnectedCallback;
        Unity.Netcode.NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnectCallback;
    }

    private void OnClientConnectedCallback(ulong clientId)
    {
        print("Server: " + clientId);
        this.players++;
        
        foreach (NetworkClient player in Unity.Netcode.NetworkManager.Singleton.ConnectedClientsList)
        {
            if (player.ClientId == clientId)
            {
                foreach (int color in availableColors)
                {
                    if (!assignedColors.Contains(color))
                    {
                        player.PlayerObject.gameObject.GetComponent<Network>().SetColor(color);
                        assignedColors.Add(color);
                        break;
                    }
                }

                UpdateColorsClientRpc();
            }
        }
    }

    private void OnClientDisconnectCallback(ulong clientId)
    {
        this.players--;
    }

    [ClientRpc]
    private void UpdateColorsClientRpc()
    {
        foreach (Network player in FindObjectsOfType<Network>())
        {
            player.SetColor(player.index.Value);
        }
    }

    public void Autodestruccion()
    {
        Destroy(this);
    }
}
