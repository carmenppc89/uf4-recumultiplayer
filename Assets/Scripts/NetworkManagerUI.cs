using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManagerUI : MonoBehaviour
{
    public GameEvent Cliente;
    public Button serverBtn;
    public Button clientBtn;
    public Button hostBtn;
    //public Button startBtn;

    private void Awake()
    {
        serverBtn.onClick.AddListener(() =>
        {
            Unity.Netcode.NetworkManager.Singleton.StartServer();
            StartLobby(true);
        });
        clientBtn.onClick.AddListener(() =>
        {
            Unity.Netcode.NetworkManager.Singleton.StartClient();
            Cliente.Raise();
            StartLobby(false);
        });
        hostBtn.onClick.AddListener(() =>
        {
            Unity.Netcode.NetworkManager.Singleton.StartHost();
            StartLobby(true);
        });
    }

    private void StartLobby(bool server)
    {
        serverBtn.gameObject.SetActive(false);
        clientBtn.gameObject.SetActive(false);
        hostBtn.gameObject.SetActive(false);
    }

}
